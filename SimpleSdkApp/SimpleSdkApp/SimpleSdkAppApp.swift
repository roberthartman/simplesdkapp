//
//  SimpleSdkAppApp.swift
//  SimpleSdkApp
//
//  Created by Robert Hartman on 8/9/21.
//

import SwiftUI

@main
struct SimpleSdkAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
