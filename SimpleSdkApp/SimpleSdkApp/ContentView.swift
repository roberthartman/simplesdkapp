//
//  ContentView.swift
//  SimpleSdkApp
//
//  Created by Robert Hartman on 8/9/21.
//

import SwiftUI
import SimpleSdk

struct SwiftUISimpleSdk: UIViewRepresentable {
    func makeUIView(context: Context) -> UIView {
        return SimpleSdk()
    }

    func updateUIView(_ uiView: UIView, context: Context) {
    }
}

struct ContentView: View {
    var body: some View {
        VStack(alignment: .center) {
            Text("SimpleSdkApp")
            SwiftUISimpleSdk()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

